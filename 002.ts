enum State {
  pending = "Pending",
  resolved = "Resolved",
  rejected = "Rejected",
};

Promise.resolve(1).then

class Aromise {
  private value: any;
  private state: State;
  public then: (value) => any;
  public catch: (value) => any;

  constructor(executor) {
    const tryCall = (callback) => Aromise.try(() => callback(this.value));
    const laterCalls = [];
    const callLater = (getMember) => (callback) =>
      new Aromise((resolve) =>
        laterCalls.push(() => resolve(getMember()(callback)))
      );
    const members = {
      [State.resolved]: {
        state: State.resolved,
        then: tryCall,
        catch: (_) => this,
      },
      [State.rejected]: {
        state: State.rejected,
        then: (_) => this,
        catch: tryCall,
      },
      [State.pending]: {
        state: State.pending,
        then: callLater(() => this.then),
        catch: callLater(() => this.catch),
      },
    };
    const changeState = (state) => Object.assign(this, members[state]);
    const apply = (value, state) => {
      if (this.state === State.pending) {
        this.value = value;
        changeState(state);
        for (const laterCall of laterCalls) {
          laterCall();
        }
      }
    };

    const getCallback = (state) => (value) => {
      if (value instanceof Aromise && state === State.resolved) {
        value.then((value) => apply(value, State.resolved));
        value.catch((value) => apply(value, State.rejected));
      } else {
        apply(value, state);
      }
    };

    const resolve = getCallback(State.resolved);
    const reject = getCallback(State.rejected);
    changeState(State.pending);
    try {
      executor(resolve, reject);
    } catch (error) {
      reject(error);
    }
  }

  static resolve(value) {
    return new Aromise((resolve) => resolve(value));
  }

  static reject(value) {
    return new Aromise((_, reject) => reject(value));
  }

  static try(callback) {
    return new Aromise((resolve) => resolve(callback()));
  }
}

Aromise.resolve(3).then(console.log);
