---
theme: "solarized"
transition: "slide"
controls: true
# highlightTheme: "monokai"
logo: false
slideNumber: false
title: "Functional programing"
width: "100%"
height: "100%"
center: false
---

## Functional reactive programming

Functional reactive programming (FRP) is a programming paradigm for reactive programming (asynchronous dataflow programming) using the building blocks of functional programming (e.g. map, reduce, filter). FRP has been used for programming graphical user interfaces (GUIs), robotics, games, and music, aiming to simplify these problems by explicitly modeling time.
https://en.wikipedia.org/wiki/Functional_reactive_programming
https://confluence.atalink.com/display/DEVELOPMENT/Web

---

## Functional programing

Functional programming languages are specially designed to handle symbolic computation and list processing applications. Functional programming is based on mathematical functions. Some of the popular functional programming languages include: Lisp, Python, Erlang, Haskell, Clojure, etc.

--

Functional programming languages are categorized into two groups, i.e. −

- **Pure Functional Languages** − These types of functional languages support only the functional paradigms. For example − Haskell.

- **Impure Functional Languages** − These types of functional languages support the functional paradigms and imperative style programming. For example − LISP.

---

## Functional Programming – Characteristics

The most prominent characteristics of functional programming are as follows

--

- Functional programming languages are designed on the concept of mathematical functions that use conditional expressions and recursion to perform computation.

- Functional programming supports higher-order functions and lazy evaluation features.

--

- Functional programming languages don’t support flow Controls like loop statements and conditional statements like If-Else and Switch Statements. They directly use the functions and functional calls.

- Like OOP, functional programming languages support popular concepts such as Abstraction, Encapsulation, Inheritance, and Polymorphism.

---

## Functional Programming – Advantages

Functional programming offers the following advantages −

--

- Bugs-Free Code − Functional programming does not support state, so there are no side-effect results and we can write error-free codes.

- Efficient Parallel Programming − Functional programming languages have NO Mutable state, so there are no state-change issues. One can program "Functions" to work parallel as "instructions". Such codes support easy reusability and testability.

--

- Efficiency − Functional programs consist of independent units that can run concurrently. As a result, such programs are more efficient.

- Supports Nested Functions − Functional programming supports Nested Functions.

- Lazy Evaluation − Functional programming supports Lazy Functional Constructs like Lazy Lists, Lazy Maps, etc.

---

## Functional Programming – Disadvantages

As a downside, functional programming requires a large memory space. As it does not have state, you need to create new objects every time to perform actions.

---

## Functional Programming – Usecase

Functional Programming is used in situations where we have to perform lots of different operations on the same set of data.

- Lisp is used for artificial intelligence applications like Machine learning, language processing, Modeling of speech and vision, etc.

- Embedded Lisp interpreters add programmability to some systems like Emacs.

---

### Functional Programming vs. Object-oriented Programming

The following table highlights the major differences between functional programming and object-oriented programming

---

| Functional Programming                 | OOP                                           |
| -------------------------------------- | --------------------------------------------- |
| Uses Immutable data.                   | Uses Mutable data.                            |
| Follows Declarative Programming Model. | Follows Imperative Programming Model.         |
| Focus is on: “What you are doing”      | Focus is on “How you are doing”               |
| Supports Parallel Programming          | Not suitable for Parallel Programming         |
| Its functions have no-side effects     | Its methods can produce serious side effects. |

--

| Functional Programming                                                    | OOP                                                                                   |
| ------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- |
| Flow Control is done using function calls & function calls with recursion | Flow control is done using loops and conditional statements.                          |
| It uses "Recursion" concept to iterate Collection Data.                   | It uses "Loop" concept to iterate Collection Data. For example: For-each loop in Java |
| Execution order of statements is not so important.                        | Execution order of statements is very important.                                      |
| Supports both "Abstraction over Data" and "Abstraction over Behavior".    | Supports only "Abstraction over Data".                                                |
