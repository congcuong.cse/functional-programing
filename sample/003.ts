type JiraIssue = {
  id: string;
  title: string;
  status: string;
};

type Data = JiraIssue & {
  children: [JiraIssue];
};

// CURRING

const filterIssues = (issues: Data[]) => (
  condition: (issue: JiraIssue) => boolean
): JiraIssue[] => {
  return issues.flatMap((issue) => {
    return (condition(issue) ? [issue] : []).concat();
  });
};

const filterDataWithCondition = filterIssues([]);
const checkStatus = (status: string) => (issue: JiraIssue): boolean => {
  return issue.status === status;
};
const filterCondition = checkStatus("resolved");
filterDataWithCondition(filterCondition);

export default {};
