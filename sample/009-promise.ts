const result = Promise.reject(2)
  .then((a) => {
    console.log("THEN1");
    return a;
  })
  .catch((err) => {
    console.log("err", err);
    return Promise.reject("ERR3");
  }) // P.reject<void>
  .then((b) => {
    console.log(b);
  }) // P.resolve<void>
  .catch((err2) => {
    console.log("err2", err2);
  }); // P.resolve<void>

console.log(result);
// result: void
