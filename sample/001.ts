type JiraIssue = {
    id: string;
    title: string;
    status: string;
}

type Data = JiraIssue & {
    children: [JiraIssue]
}

const filterIssues = (issues: Data[], condition: (issue: JiraIssue) => boolean): JiraIssue[] => {
    const res: JiraIssue[] = [];
    
    for (const issue of issues) {
        if (condition(issue)) {
            res.push(issue);
        }
        for (const child of issue.children) {
            if (condition(child)) {
                res.push(child);
            }
        }
    }
    return res;
}

filterIssues([], (issue) => issue.status === 'resolved')

export default {}
