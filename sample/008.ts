const a = 1;

const add = (value: number) => (data: number) => data + value;
const multiple = (value: number) => (data: number) => data * value;
const division =
  (value: number) =>
  (data: number): Promise<number> => {
    if (value === 0) {
      return Promise.reject("");
    }

    return Promise.resolve(data / value);
  };

Promise.resolve(3)
  .then(add(2))
  .then((x) => Promise.resolve(multiple(4)(x)));

Promise.resolve(a).then(add(4)).then(multiple(2)).then(division(0));

export default {};
