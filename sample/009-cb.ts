const cb = (task: Function, callback: (data, err) => void) => {
  try {
    const data = task();
    callback(data, null);
  } catch (e) {
    callback(null, e);
  }
};

const func2 = (funcCb: (data, error) => void) => {
  cb(
    () => {
      throw 2;
    },
    (a, err) => {
      if (err) {
        console.log("err", err);
        cb(
          () => {
            throw "ERR3";
          },
          (a, err2) => {
            if (err2) {
              console.log("err2", err2);
              funcCb(undefined, null);
            } else funcCb(a, null);
          }
        );
      } else {
        console.log("THEN1");
        cb(
          () => {
            return a;
          },
          (b, err1) => {
            if (err1) {
              console.log(b);
              funcCb(undefined, null);
            } else funcCb(b, null);
          }
        );
      }
    }
  );
};

func2((result1, error) => {
  console.log(result1, error);
});
