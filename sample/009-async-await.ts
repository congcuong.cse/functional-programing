const func = async () => {
  try {
    try {
      const a = await Promise.reject(2);
      console.log("THEN1");
      const b = await Promise.resolve(a);
      console.log(b);
    } catch (err) {
      console.log("err", err);
      await Promise.reject("ERR3");
    }
  } catch (err2) {
    console.log("err2", err2);
  }
};
const result1 = func().then();
console.log(result1);
