import { compose, pipe, curry } from "lodash/fp";

const doA = (a: string): string | null => {
  return null;
};

const doB = (b: string): string | null => {
  return null;
};

const doC = (c: string): string | null => {
  return null;
};

const doAll = () => {
  const defaultValue = "default";
  const a = "1";
  const b = doA(a);
  if (b) {
    const c = doC(b);
    if (c) {
      if (c) {
        return c;
      }
      return defaultValue;
    }
    return defaultValue;
  }
  return defaultValue;
};

const doAll_ = () => {
  const a = "1";
  return Maybe.just(a).map(doA).map(doB).map(doC).getValueOr("default");
};

a?.doA()?.doB()?.doC() ?? "default";

const doAll2 = () => {
  const defaultValue = "default";
  const a = "1";
  const b = doA(a);
  if (b) {
    const c = doC(b);
    if (c) {
      if (c) {
        return c;
      }
      return defaultValue;
    }
    return defaultValue;
  }
  return defaultValue;
};

const doAll2_ = () => {
  const a = "1";
  return Maybe.just(a)
    .map((x) => Maybe.just(x).map(doA).getValueOr("defaultA"))
    .map((x) => Maybe.just(x).map(doB).getValueOr("defaultB"))
    .map((x) => Maybe.just(x).map(doC).getValueOr("defaultC"))
    .getValue()!;
};

const add2 = (a, b) => a + b;
const add3 = (a) => (b) => a + b; //CURRING
const add4 = curry(add2);

const add = (value: number) => (data: number) => data + value;
const multiple = (value: number) => (data: number) => data * value;
const division =
  (value: number) =>
  (data: number): number => {
    return data / value;
  };

Maybe.nothing<number>()
  .map(add(3)) // i => i + 3
  .map(multiple(4))
  .map(division(0))
  .getValue();

Maybe.just(1).map(compose(division(0), multiple(4), add(3)));
Maybe.just(1).map(pipe(add(3), multiple(4), division(0)));

// g.f: compose
// g;f : pipe
