type JiraIssue = {
  id: string;
  title: string;
  status: string;
};

type Data = JiraIssue & {
  children: [JiraIssue];
};

const filterIssues = (
  issues: Data[],
  condition: (issue: JiraIssue) => boolean
): JiraIssue[] => {
  return issues.flatMap((issue) => {
    return (condition(issue)
      ? [{ id: issue.id, title: issue.title, status: issue.status }]
      : []
    ).concat(issue.children.filter(condition));
  });
};

filterIssues([], (issue) => issue.status === "resolved");

export default {};
