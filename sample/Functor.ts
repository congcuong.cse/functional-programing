interface Functor<T> {
  map<U>(f: (x: T) => U): Functor<U>;
}

/*

1. Identity law
When map is called on a Functor with identity function, you get the Functor back.
Functor[X].map(x => identity(x)) == Functor[X]
or
Functor[X].map(identity) == Functor[X]


Functor[X].map(f).map(g) == Functor[X].map(x => g(f(x))

*/

function fmap<T, U>(f: (arg: T) => U): (Fx: Functor<T>) => Functor<U> {
  return (Fx) => Fx.map(f);
}

[1, 2].map((i) => `${i * 2}`);
