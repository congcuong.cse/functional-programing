type JiraIssue = {
    id: string;
    title: string;
    status: string;
}

type Data = JiraIssue & {
    children: [JiraIssue]
}

const filterIssues = (condition: (issue: JiraIssue) => boolean) => (issues: Data[]): JiraIssue[] => {
    return issues.flatMap((issue) => {
        return (condition(issue) ? [issue] : []).concat()
    })
}

const checkStatus = (status: string) => (issue: JiraIssue): boolean => {
    return issue.status === status;
}

const filterFunc = filterIssues(checkStatus('resolved'))

filterFunc([])

export default {}
