class Maybe<T> implements Functor<T>, Monad<T> {
  static just = <T>(x: T) => new Maybe<T>(x);
  static nothing = <T>() => new Maybe<T>(null);

  protected value: { val: T | null; type: "just" } | { type: "nothing" };
  constructor(x: T | null) {
    if (x !== null) this.value = { val: x, type: "just" };
    else this.value = { type: "nothing" };
  }

  map<U>(f: (x: T) => U | null): Maybe<U> {
    if (this.value.type === "just") {
      return new Maybe(f(this.value.val!));
    } else {
      return Maybe.nothing();
    }
  }

  flatMap<U>(f: (x: T) => Maybe<U>): Maybe<U> {
    if (this.value.type === "just") {
      return f(this.value.val!);
    } else {
      return Maybe.nothing();
    }
  }

  getValue = (): T | null => {
    if (this.value.type === "just") {
      return this.value.val!;
    } else {
      return null;
    }
  };

  getValueOr = (defaultValue: T): T => {
    if (this.value.type === "just") {
      return this.value.val!;
    } else {
      return defaultValue;
    }
  };

  toString(): string {
    if (this.value.type === "just") return `just(${this.value.val})`;
    else return `nothing()`;
  }
}
