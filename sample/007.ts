const a = 1;

const add = (value: number) => (data: number) => data + value;
const multiple = (value: number) => (data: number) => data * value;
const division = (value: number) => (data: number): Maybe<number> => {
  if (value === 0) {
    return Maybe.nothing();
  }

  return Maybe.just(data / value);
};

Maybe.just(a).map(add(3)).map(multiple(4)).flatMap(division(0)).getValue();

export default {};
