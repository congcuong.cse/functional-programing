interface Monad<T> {
  flatMap<U>(f: (x: T) => Monad<U>): Monad<U>;
}

/*

1. Left-identity law
unit(x).flatMap(f) == f(x)

2. Right-identity law
Monad[X].flatMap(unit) == Monad[X]

3. Associativity law
m.flatMap(f).flatMap(g) == m.flatMap(x ⇒ f(x).flatMap(g))

*/

function flatMap<T, U>(f: (arg: T) => Monad<U>): (Fx: Monad<T>) => Monad<U> {
  return (Fx) => Fx.flatMap(f);
}

[1, 2].flatMap((i) => [`${i}`, `${i * 2}`]);
