class Either<T> implements Functor<T> {
  value: { msg: string; type: "left" } | { type: "right"; val: T };
  constructor(msg: string);
  constructor(msg: null, x: T);
  constructor(msg: string | null, x?: T) {
    if (msg === null)
      // @ts-ignore
      this.value = { val: x, type: "right" };
    else this.value = { type: "left", msg };
  }

  map<U>(f: (x: T) => U): Functor<U> {
    if (this.value.type === "right") return right(f(this.value.val));
    else return left(this.value.msg);
  }

  toString(): string {
    if (this.value.type === "right") return `right(${this.value.val})`;
    else return `left(${this.value.msg})`;
  }
}

const left = <T>(msg: string) => new Either<T>(msg);
const right = <T>(val: T) => new Either<T>(null, val);
